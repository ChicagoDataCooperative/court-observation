# Bond Court Observation

This data has been deposited by Chicago Appleseed.

1. data_processing.Rmd and data_processing.html. These are an R-Markdown file that I used to process the data from its raw form into cleaned up files. It shows all the code I used and explains how variables were re-coded and combined. It also explains what all the variable names mean. 
   The html file should be open-able in a browser and is the nice to look at document. It has short explanations of variables followed by chunks of R code. Skip the code if reading it is confusing. Just know that generally the format of the file is that I explain what I did and then there is code that does the thing.
   The Rmd is the document that produced the .html and all of the processed data files.
2. Processed data for both months with one line per observation form (i.e. some IDs have more than one line). These are aug_data_by_entry.csv and sept_oct_data_by_entry.csv. These files are produced by the code in 1. These don't contain identifying information and can be shared.
3. Processed data with one row per individual. This file is aug_sept_oct_by_id.csv. This also doesn't contain any identifying information and can be shared.
